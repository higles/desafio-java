package com.bancointer.desafiojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioBancoInterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioBancoInterApplication.class, args);
	}

}
