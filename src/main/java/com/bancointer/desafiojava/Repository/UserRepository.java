package com.bancointer.desafiojava.Repository;

import com.bancointer.desafiojava.Model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserModel,Integer> {
    @Query("from User a where a.id=:userID")
    Optional<UserModel> findById(@Param("userID") Long userid);
}
