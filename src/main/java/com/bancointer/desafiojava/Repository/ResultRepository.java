package com.bancointer.desafiojava.Repository;
import com.bancointer.desafiojava.Model.ResultModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ResultRepository extends JpaRepository<ResultModel,Integer>{

    /***
     * Implementa a busca de todos os resultados filtrando pelo ID do usuário.
     * @param userid
     * @return
     */
    @Query("from Result a where a.user.id=:userID")
    public java.util.Optional<List<ResultModel>> findByUser(@Param("userID") Long userid);
}
