package com.bancointer.desafiojava.Controller;

import com.bancointer.desafiojava.Exception.CreateUserException;
import com.bancointer.desafiojava.Exception.InvalidDataResultException;
import com.bancointer.desafiojava.Exception.UserNotFoundException;
import com.bancointer.desafiojava.Model.ResultModel;
import com.bancointer.desafiojava.Service.ResultService;
import com.bancointer.desafiojava.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(path = "/api/result")
@RestController
@Api(value = "API REST de resultados dos cálculos de dígito único")
public class ResultController {
    @Autowired
    private ResultService resultService;

    @Autowired
    private UserService userService;

    @GetMapping(path = "/get-by-user/{userid}")
    @ApiOperation(value = "Retorna uma lista de resultados de cálculos de dígito único filtrando por usuário")
    public ResponseEntity<Optional<List<ResultModel>>> GetByUser(@PathVariable("userid") Long userId) {
        return ResponseEntity.ok().body(resultService.findByUser(userId));
    }

    @PostMapping(path = "/new")
    @ApiOperation(value = "Cria um novo cálculo de dígito único")
    public ResponseEntity<Long> UserSingleDigit(@RequestBody ResultModel result) {
        resultService.singleDigit(result);
        if (null != result.getUser()) {
            userService.findById(result.getUser().getId());
            resultService.save(result);
        }
        return ResponseEntity.ok().body(result.getResultDigit());
    }

    @GetMapping(path = "/find-all")
    @ApiOperation(value = "Retorna uma lista com todos os resultados de cálculos de dígito único ")
    public ResponseEntity findAll() {
        return ResponseEntity.ok().body(resultService.findAll());
    }

    @ExceptionHandler(InvalidDataResultException.class)
    public ResponseEntity<String> dataResultError() {
        return ResponseEntity.badRequest().body("Falha ao criar dígito único pois as informações não foram preenchidas corretamente");
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> userNotFoundError() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não foi possível realizar a operação, pois o usuário informado não existe");
    }
}