package com.bancointer.desafiojava.Controller;

import com.bancointer.desafiojava.Exception.CreateUserException;
import com.bancointer.desafiojava.Exception.EncryptException;
import com.bancointer.desafiojava.Exception.PublicKeyException;
import com.bancointer.desafiojava.Exception.UserNotFoundException;
import com.bancointer.desafiojava.Model.UserModel;
import com.bancointer.desafiojava.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path = "/api/user")
@RestController
@Api(value = "API REST de usuários dos cálculos de dígito único")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/{userId}")
    @ApiOperation(value = "Busca um usuário")
    public ResponseEntity getUser(@PathVariable("userId") Long userId) {
        return userService.findById(userId)
                .map(user -> ResponseEntity.ok().body(user))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/new")
    @ApiOperation(value = "Adiciona um usuário")
    public UserModel createUser(@RequestBody UserModel user) {
        return userService.create(user);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Atualiza um usuário")
    public ResponseEntity<UserModel> update(@RequestBody UserModel user) {
        return ResponseEntity.ok().body(userService.update(user));
    }

    @GetMapping(path = "/find-all")
    @ApiOperation(value = "Busca todos os usuário")
    public ResponseEntity findAll() {
        return ResponseEntity.ok().body(userService.findAll());
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Apaga um usuário")
    public ResponseEntity<Long> delete(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(userService.delete(id));
    }

    @ExceptionHandler(PublicKeyException.class)
    public ResponseEntity<String> publicKeyError() {
        return ResponseEntity.badRequest().body("Aconteceu um erro ao utilizar a chave publica. Por favor, revise a chave enviada e tente novamente.");
    }

    @ExceptionHandler(EncryptException.class)
    public ResponseEntity<String> encryptError() {
        return ResponseEntity.badRequest().body("Aconteceu um erro ao criptografar os dados. Por favor, tente novamente.");
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> userNotFoundError() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não foi possível realizar a operação, pois o usuário informado não existe");
    }

    @ExceptionHandler(CreateUserException.class)
    public ResponseEntity<String> createUserError() {
        return ResponseEntity.badRequest().body("Falha ao criar usuário pois nem todas as informações foram preenchidas corretamente");
    }
}