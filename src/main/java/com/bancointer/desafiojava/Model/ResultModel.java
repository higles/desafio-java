package com.bancointer.desafiojava.Model;

import com.bancointer.desafiojava.Exception.InvalidDataResultException;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Result")
public class ResultModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String digit;

    @Column
    private Long multiplier;

    @Column
    private Long resultDigit;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserModel user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }

    public Long getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Long multiplier) {
        this.multiplier = multiplier;
    }

    public Long getResultDigit() {
        return resultDigit;
    }

    public void setResultDigit(Long resultDigit) {
        this.resultDigit = resultDigit;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public boolean multiplierExists() {
        return (null != this.multiplier);
    }

    public boolean IsValid() {
        return (multiplier == null || (multiplier > 0 && multiplier <= 100000)) && digitIsValid();
    }

    private boolean digitIsValid() throws InvalidDataResultException {
        try {
            char[] charArray;
            charArray = digit.toCharArray();
            int number = 0;
            for (char item : charArray) {
                number = Integer.parseInt(String.valueOf(item));
            }
        } catch (Exception ex) {
            throw new InvalidDataResultException("O digito contem caracteres invalidos");
        }
        return this.digit != "";
    }
}
