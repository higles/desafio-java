package com.bancointer.desafiojava.Model;

import com.bancointer.desafiojava.Exception.PublicKeyException;
import com.bancointer.desafiojava.Service.SecurityService;

import javax.persistence.*;
import java.io.Serializable;
import java.security.PublicKey;

@Entity(name = "User")
public class UserModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 2048)
    private String name;

    @Column(nullable = false, length = 2048)
    private String email;

    @Column(nullable = false, length = 2048)
    private String userPublicKey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserPublicKey() {
        return userPublicKey;
    }

    public PublicKey RsaUserPublicKey() throws PublicKeyException {
        return SecurityService.parsePublicKey(userPublicKey);
    }

    public void setUserPublicKey(String userPublicKey) {
        this.userPublicKey = userPublicKey;
    }

    public boolean userIsValid() {
        return this.name != null && this.email != null && this.userPublicKey != null;
    }
}
