package com.bancointer.desafiojava.Service;

import com.bancointer.desafiojava.Exception.EncryptException;
import com.bancointer.desafiojava.Exception.PublicKeyException;
import org.springframework.stereotype.Service;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class SecurityService {
    public static final String ALGORITHM = "RSA";

    /***
     * Método responsável por criptografar os dados
     * @param plainText Texto a ser criptografado
     * @param publicKey Chave publica do usuário
     * @return Texto criptografado
     * @throws PublicKeyException Exceção ao usar a chave publica
     * @throws EncryptException Exceção ao encriptar o texto
     */
    public static String encrypt(String plainText, PublicKey publicKey) throws PublicKeyException, EncryptException {
        try {
            Cipher encryptCipher = Cipher.getInstance(ALGORITHM);
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] cipherText = encryptCipher.doFinal(plainText.getBytes(UTF_8));
            return Base64.getEncoder().encodeToString(cipherText);
        } catch (InvalidKeyException ex) {
            throw new PublicKeyException("Chave Publica do usuário é inválida: " + ex.toString());
        } catch (NoSuchAlgorithmException ex) {
            throw new EncryptException("Erro usar o algoritivo RSA " + ex.toString());
        } catch (NoSuchPaddingException ex) {
            throw new EncryptException("Algoritimo RSA não foi definido " + ex.toString());
        } catch (BadPaddingException ex) {
            throw new EncryptException("Algoritimo RSA é inválido " + ex.toString());
        } catch (IllegalBlockSizeException ex) {
            throw new EncryptException("Tamanho do texto é invalido " + ex.toString());
        }
    }

    /***
     * Transforma a Chave Publica String em um objeto;
     * @param publicKey Chave Publica em String
     * @return Chave Publica objeto
     * @throws PublicKeyException Exceção ao usar a chave publica
     */
    public static PublicKey parsePublicKey(String publicKey) throws PublicKeyException {
        try {
            String pubKeyPEM = publicKey.replace("-----BEGIN PUBLIC KEY-----\n", "").replace("-----END PUBLIC KEY-----", "");
            byte[] encodedPublicKey = org.bouncycastle.util.encoders.Base64.decode(pubKeyPEM);
            X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);

        } catch (InvalidKeySpecException ex) {
            throw new PublicKeyException("Chave Publica do usuário é inválida: " + ex.toString());
        } catch (NoSuchAlgorithmException ex) {
            throw new PublicKeyException("Erro usar o algoritivo RSA " + ex.toString());
        } catch (Exception ex) {
            throw new PublicKeyException("Erro ao converter a Chave Publica do usuário: " + ex.toString());
        }
    }
}
