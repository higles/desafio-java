package com.bancointer.desafiojava.Service;

import com.bancointer.desafiojava.Exception.CreateUserException;
import com.bancointer.desafiojava.Exception.EncryptException;
import com.bancointer.desafiojava.Exception.PublicKeyException;
import com.bancointer.desafiojava.Exception.UserNotFoundException;
import com.bancointer.desafiojava.Model.UserModel;
import com.bancointer.desafiojava.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    /**
     * Busca um usuário pelo ID
     *
     * @param userId ID do usuário
     * @return Objeto de usuário
     * @throws UserNotFoundException Cria uma exceção caso o usuário não exista
     */
    public Optional<UserModel> findById(Long userId) throws UserNotFoundException {
        Optional<UserModel> user = repository.findById(userId);
        if (user.isPresent()) {
            return user;
        } else {
            throw new UserNotFoundException("Não foi possivel encontrar o usuário pelo ID informado");
        }
    }

    /**
     * Cria um usuário
     *
     * @param user Usuário a ser criado
     * @return usuário que foi criado
     * @throws PublicKeyException  Erro na chave publica do usuário
     * @throws EncryptException    Erro ao criptografar os dados
     * @throws CreateUserException Erro ao criar usuário devido aos dados preechidos estarem incorretos
     */
    public UserModel create(UserModel user) throws PublicKeyException, EncryptException, CreateUserException {
        if (user.userIsValid()) {
            String encryptedName = SecurityService.encrypt(user.getName(), user.RsaUserPublicKey());
            String encryptedEmail = SecurityService.encrypt(user.getEmail(), user.RsaUserPublicKey());
            user.setName(encryptedName);
            user.setEmail(encryptedEmail);
            return repository.save(user);
        } else {
            throw new CreateUserException("Falha ao criar usuário pois nem todas as informações foram preenchidas corretamente");
        }
    }

    /**
     * @param user Usuário a ser atualizado
     * @return usuário atualizado
     * @throws PublicKeyException    Erro na chave publica
     * @throws EncryptException      Erro ao criptografar os dados
     * @throws UserNotFoundException Usuário não encontrado
     * @throws CreateUserException   Erro ao criar usuário devido aos dados preechidos estarem incorretos
     */
    public UserModel update(UserModel user) throws PublicKeyException, EncryptException, UserNotFoundException {
        Optional<UserModel> searchedUser = repository.findById(user.getId());
        if (searchedUser.isPresent()) {
            String encryptedName = SecurityService.encrypt(user.getName(), searchedUser.get().RsaUserPublicKey());
            String encryptedEmail = SecurityService.encrypt(user.getEmail(), searchedUser.get().RsaUserPublicKey());
            searchedUser.get().setName(encryptedName);
            searchedUser.get().setEmail(encryptedEmail);
            return repository.save(searchedUser.get());
        } else {
            throw new UserNotFoundException("Não foi possivel encontrar o usuário pelo ID informado");
        }
    }

    /***
     * Busca todos os usuários
     * @return Lista com todos os usuários
     */
    public List<UserModel> findAll() {
        return repository.findAll();
    }

    /***
     * Apaga um usuário pelo ID
     * @param id ID do usuário a ser apagado
     * @return ID do usuário apagado
     * @throws UserNotFoundException Usuário não encontrado
     */
    public Long delete(Long id) throws UserNotFoundException {
        Optional<UserModel> user = findById(id);
        if (user.get() != null) {
            repository.delete(user.get());
            return user.get().getId();
        } else {
            throw new UserNotFoundException("Não foi possivel encontrar o usuário pelo ID informado");
        }
    }
}
