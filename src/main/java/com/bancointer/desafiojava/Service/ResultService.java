package com.bancointer.desafiojava.Service;

import com.bancointer.desafiojava.Exception.InvalidDataResultException;
import com.bancointer.desafiojava.Model.ResultModel;
import com.bancointer.desafiojava.Repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ResultService {
    @Autowired
    private ResultRepository repository;

    @Autowired
    private UserService userService;

    private static final Map<String, ResultModel>  cacheResult = new HashMap<>();

    /***
     * Realiza calculo do digito unico
     * @param digit digito a ser calculado
     * @return retorna o digito unico
     */
    public Long singleDigit(String digit) {
        char[] charArray;
        charArray = digit.toCharArray();
        long singleDigit = 0L;
        if (charArray.length > 1) {
            for (char item : charArray) {
                singleDigit += Integer.parseInt(String.valueOf(item));
            }
            return singleDigit(Long.toString(singleDigit));
        } else {
            return Long.parseLong(digit);
        }
    }

    /***
     * Calculo do digito unico, verifica se já existe o resultado em cache e chama o metodo principal
     * @param result ResultModel Objeto com os parametros para o calculo do digito unico
     * @return Objeto ResultModel com o digito unico calculado
     */
    public ResultModel singleDigit(ResultModel result) {
        String digit = addMultiplierString(result);
        Optional<ResultModel> resultCache = findResultCache(digit);

        if ((resultCache.isPresent())) {
            result.setResultDigit(resultCache.get().getResultDigit());
        } else {
            result.setResultDigit(singleDigit(digit));
            addResultCache(digit, result);
        }
        return result;
    }

    /***
     * Verifica se existe um multiplicador e cria o digito
     * @param result Objeto com dados a serem calculados
     * @return digito multiplicado
     */
    public String addMultiplierString(ResultModel result) {
       if(!result.IsValid()){
           throw new InvalidDataResultException("Não é permitido usar numero negativo para o multiplicador");
       }else if (result.multiplierExists()) {
            String stringSingleDigit = "";
            for (int i = 0; i < result.getMultiplier(); i++) {
                stringSingleDigit = stringSingleDigit.concat(result.getDigit());
            }
            return stringSingleDigit;
        } else {
            return result.getDigit();
        }
    }

    /***
     * Adiciona resutlado no cache
     * @param digit digito a ser adicionado no cache
     * @param result objeto do resultado do calculo do digito unico
     */
    public void addResultCache(String digit, ResultModel result) {
        if (cacheResult.size() >= 10) {
            cacheResult.remove(digit);
        }
        cacheResult.put(digit, result);
    }

    /***
     * Busca o resultado do calculo do digito unico no cache
     * @param digit digito a ser procurado no cache
     * @return Objeto do resultado do calculo do digito unico armazenado no cache
     */
    public Optional<ResultModel> findResultCache(String digit) {
        return Optional.ofNullable(cacheResult.get(digit));
    }

    /***
     * Busca todos os resultados pelo id do usuário
     * @param userId ID do usuário
     * @return Lista de resultados
     */
    public Optional<List<ResultModel>> findByUser(Long userId) {
        userService.findById(userId);
        return repository.findByUser(userId);
    }

    /***
     * Busca todos os resultados
     * @return Lista de resultados
     */
    public List<ResultModel> findAll() {
        return repository.findAll();
    }

    /***
     * Cria um novo usuário
     * @param result Resultado a ser adicionado
     * @return resultado adicionado
     */
    public ResultModel save(ResultModel result) {
        return repository.save(result);
    }
}
