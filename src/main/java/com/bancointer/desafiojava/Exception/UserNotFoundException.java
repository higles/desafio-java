package com.bancointer.desafiojava.Exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String ex){
        super(ex);
    }
}
