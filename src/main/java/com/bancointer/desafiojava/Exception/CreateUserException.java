package com.bancointer.desafiojava.Exception;

public class CreateUserException extends RuntimeException{
    public CreateUserException(String ex){
        super(ex);
    }
}
