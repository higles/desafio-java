package com.bancointer.desafiojava.Exception;

public class InvalidDataResultException extends RuntimeException {
    public InvalidDataResultException(String ex) {
        super(ex);
    }
}