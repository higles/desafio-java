package com.bancointer.desafiojava.Exception;

public class EncryptException extends RuntimeException{
    public EncryptException(String ex){
        super(ex);
    }
}
