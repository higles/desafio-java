package com.bancointer.desafiojava.Exception;

public class PublicKeyException extends RuntimeException {
    public PublicKeyException(String ex){
        super(ex);
    }
}
