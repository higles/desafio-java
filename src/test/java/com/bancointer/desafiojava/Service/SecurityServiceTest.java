package com.bancointer.desafiojava.Service;

import com.bancointer.desafiojava.Exception.PublicKeyException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.security.PublicKey;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
public class SecurityServiceTest {

    @Test
    public void getPublicKeyTest() {
        String publicKey = ("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        assertDoesNotThrow(()->SecurityService.parsePublicKey(publicKey));
    }

    @Test
    public void invalidPublicKeyTest() {
        String publicKey = ("-----BEGIN PUBLIC KEY-----\n" +
                "------------------------INVALID_PUBLIC_KEY----------------------\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        assertThrows(PublicKeyException.class,()->SecurityService.parsePublicKey(publicKey));
    }

    @Test
    public void encryptTest() {
        String publicKey = ("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        PublicKey key = SecurityService.parsePublicKey(publicKey);
        assertDoesNotThrow(()->SecurityService.encrypt("Texto",key));
    }
}
