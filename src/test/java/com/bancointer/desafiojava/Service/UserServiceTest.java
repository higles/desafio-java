package com.bancointer.desafiojava.Service;


import com.bancointer.desafiojava.Exception.PublicKeyException;
import com.bancointer.desafiojava.Exception.UserNotFoundException;
import com.bancointer.desafiojava.Model.UserModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@WebAppConfiguration
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void createUserTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        userService.create(user);
        assertNotNull(user);
        assertNotNull(user.getId());
        assertNotNull(user.getName());
        assertNotNull(user.getEmail());
        assertNotNull(user.getUserPublicKey());
        assertTrue(user.userIsValid());
    }

    @Test
    public void userInvalidKeyTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("Invalid Public Key");
        assertThrows(PublicKeyException.class, () -> userService.create(user));
    }

    @Test
    public void findByIdTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");

        Long userID = userService.create(user).getId();
        UserModel userTest = userService.findById(userID).get();

        assertNotNull(userTest);
        assertNotNull(userTest.getId());
        assertNotNull(userTest.getName());
        assertNotNull(userTest.getEmail());
        assertNotNull(userTest.getUserPublicKey());
        assertTrue(userTest.userIsValid());
    }

    @Test
    public void findAllTest() {
        String publicKey = ("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");

        Random random = new Random();

        //gera 100 resultados aleatorios
        for (int i = 0; i < 100; i++) {
            //Criando usuário
            UserModel user = new UserModel();
            user.setName("Higles Souza " + i);
            user.setEmail("Higlessouza@gmail.com " + i);
            user.setUserPublicKey(publicKey);
            userService.create(user);
            random.nextInt(100);
        }
        List<UserModel> listUserTest = userService.findAll();

        listUserTest.forEach(userModel -> {
            assertNotNull(userModel);
            assertNotNull(userModel.getName());
            assertNotNull(userModel.getEmail());
            assertNotNull(userModel.getUserPublicKey());
            assertNotNull(userModel.getId());
        });
        assertTrue(listUserTest.size() >= 100);
    }

    @Test
    public void deleteUserTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        Long id = userService.create(user).getId();
        //Usuario existe na base de dados antes de ser apagado
        assertDoesNotThrow(() -> userService.findById(id));

        //Apagando usuário
        userService.delete(id);

        //usuário foi apagado com sucesso
        assertThrows(UserNotFoundException.class, () -> userService.findById(id));
    }

    @Test
    public void updateUserTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        userService.create(user);
        user.setEmail("higles@hotmail.com");
        user.setName("Higles Santos");
        assertDoesNotThrow(() -> userService.update(user));
    }

    @Test
    public void findByIdNotFond() {
        assertThrows(UserNotFoundException.class, () -> userService.findById(1999L));
    }
}
