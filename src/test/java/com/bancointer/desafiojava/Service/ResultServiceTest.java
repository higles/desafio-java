package com.bancointer.desafiojava.Service;

import com.bancointer.desafiojava.Exception.InvalidDataResultException;
import com.bancointer.desafiojava.Model.ResultModel;
import com.bancointer.desafiojava.Model.UserModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@WebAppConfiguration
public class ResultServiceTest {

    @Autowired
    private ResultService resultService;

    @Autowired
    private UserService userService;

    public ResultModel resultSample() {
        ResultModel newResult = new ResultModel();
        newResult.setDigit("9875");
        newResult.setMultiplier(4L);
        return newResult;
    }

    @Test
    public void singleDigitTest() {
        ResultModel resultTest = resultService.singleDigit(resultSample());
        assertEquals(8L, resultTest.getResultDigit());
    }

    @Test
    public void resultMultipleNegativeTest() {
        ResultModel resultTest = new ResultModel();
        resultTest.setDigit("9875");
        resultTest.setMultiplier(-1L);
        assertThrows(InvalidDataResultException.class,() ->resultService.singleDigit(resultTest));
    }

    @Test
    public void nullDigitTest() {
        ResultModel resultTest = new ResultModel();
        resultTest.setDigit(null);
        resultTest.setMultiplier(4L);
        assertThrows(InvalidDataResultException.class,() ->resultService.singleDigit(resultTest));
    }

    @Test
    public void invalidDigitTest() {
        ResultModel resultTest = new ResultModel();
        resultTest.setDigit("9875A");
        resultTest.setMultiplier(4L);
        assertThrows(InvalidDataResultException.class,() ->resultService.singleDigit(resultTest));
    }

    @Test
    public void addMultiplierStringTest() {
        String resultTest = resultService.addMultiplierString(resultSample());
        assertEquals("9875987598759875", resultTest);
    }

    @Test
    public void saveResultTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        Long userID =  userService.create(user).getId();

        //Associando usuário ao teste
        ResultModel result = resultSample();
        result.setUser(user);
        resultService.singleDigit(result);
        ResultModel resultTest = resultService.save(result);

        assertNotNull(resultTest);
        assertEquals(8L, resultTest.getResultDigit());
    }

    @Test
    public void findByUserTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
       Long userID =  userService.create(user).getId();

        //Associando usuário ao teste
        ResultModel result = resultSample();
        result.setUser(user);
        resultService.singleDigit(result);
        resultService.save(result);

        //Buscando pelo id
        Optional<List<ResultModel>> resultTest = resultService.findByUser(userID);

        assertNotNull(resultTest);
        assertEquals(1, resultTest.get().size());
        assertEquals(8L, resultTest.get().get(0).getResultDigit());
    }

    @Test
    public void findAllTest() {
        //Criando usuário
        UserModel user = new UserModel();
        user.setName("Higles Souza");
        user.setEmail("Higlessouza@gmail.com");
        user.setUserPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmkkV/xYSQFh+p7M0oW64\n" +
                "0xMtKWRjbVqOt9q3k2GpBGXFWQBEkl/s1QBlqfd3VP4vds8gov9Ubc/aUrgl8NjW\n" +
                "LG+WWbZyd8VjYzoNdzmLuILzhfM6cnx6tyQVDSpCWL41/wLoZtGbnr6qDQzYZ/jv\n" +
                "/Mzh29RTTJR8uyXKzhBJ85uRjYfNF9EIlkWpwKFUGKEqtTfIRb7xaKSjKPcc2O4h\n" +
                "knMHuYl7YYZ/+GiCiusdwM0QnspDRwkfC9602OXuR+x9xffLJ9c7L0HBgLYLgYlM\n" +
                "eBOTS+DNGlMSstV7iIvcb83S2VOXoJqXO682XyHx/nE88uHKI0t3OlYR+L80nv+f\n" +
                "EwIDAQAB\n" +
                "-----END PUBLIC KEY-----");
        Long userID =  userService.create(user).getId();
        Random random = new Random();

        //gera 100 resultados aleatorios
        for (int i = 0; i < 100; i++) {
            ResultModel result = resultSample();
            result.setMultiplier((long) random.nextInt(600));
            result.setUser(user);
            resultService.singleDigit(result);
            resultService.save(result);
        }
        List<ResultModel> resultTest = resultService.findAll();

        resultTest.forEach(resultModel -> {
            assertNotNull(resultModel);
            assertNotNull(resultModel.getResultDigit());
            assertNotNull(resultModel.getDigit());
            assertNotNull(resultModel.getId());
        });
        assertTrue( resultTest.size()>=100);
    }

}
