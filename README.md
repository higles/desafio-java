# Desafio Java - Banco Inter
Desafio java do processo seletivo do Banco Inter.
### Compilar aplicação 
```sh
mvnw clean install
```
### Executar aplicação 
```sh
mvnw spring-boot:run
```
### Executar testes unitários  
```sh
mvnw test
```
### Swagger
```sh
http://localhost:8080/swagger-ui.html
```

# Base de dados
Foi utilizado o Hsqldb como banco de dados em cache.

# Organização do projeto

### Model 
  - ResultModel
  - UserModel 
 
 Responsável por gerenciar o modelo de dados e implementa o serializable para retornar os dados para o usuário.

### Controller 
  - ResultController
  - UserController 

Responsável por receber as chamadas REST da API. 

### Service 
  - ResultService
  - UserService
  - SecuriryService
  
Responsável pelas regras do negócio, onde é realizado o cálculo do dígito único, além de criptografar as informações de nome e e-mail do usuário, e converter a chave pública RSA.

### Repository 
  - ResultRepository
  - UserRepository 

Responsável por fazer a integração com o banco de dados.

### Config 
  - SwaggerConfig 

Implementa as configurações do Swagger.

### Exception
  - CreateUserException
  - EncryptException
  - InvalidDataResultException
  - PublicKeyException
  - UserNotFoundException

Exceções criadas de acordo com as necessidades da aplicação; 

### Test.Service
  - ResultServiceTest
  - UserServiceTest
  - SecurityServiceTest

Responsável por realizar os testes unitários da aplicação com JUnit.